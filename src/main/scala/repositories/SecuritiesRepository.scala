package repositories

import cats.effect.Sync
import cats.syntax.all.*

import datamodels.security.Security
import datamodels.security.responses.DeleteSecurityResponses
import datamodels.security.responses.DeleteSecurityResponses.DeleteSecurityError
import datamodels.security.responses.PutSecurityResponses
import datamodels.security.responses.PutSecurityResponses.PutSecurityError
import datamodels.security.responses.PutSecurityResponses.SecurityAlreadyExists
import doobie.*
import doobie.implicits.*
import doobie.postgres.*

trait SecuritiesRepository[F[_]: Sync] {
  def getSecurityById(id: Int): F[Option[Security]]

  def getSecurityBySecId(secId: String): F[Option[Security]]

  def putSecurity(security: Security): F[Either[PutSecurityError, Unit]]

  def deleteSecurity(id: Int): F[Either[DeleteSecurityError, Unit]]
}

object SecuritiesRepository {
  def apply[F[_]: Sync](implicit xa: Transactor[F]): SecuritiesRepository[F] = new SecuritiesRepository[F] {

    override def getSecurityById(id: Int): F[Option[Security]] =
      sql"select $fullSecurityFields from $securitiesTableName where id = $id"
        .query[Security]
        .option
        .transact(xa)

    override def getSecurityBySecId(secId: String): F[Option[Security]] =
      sql"select $fullSecurityFields from $securitiesTableName where sec_id = $secId"
        .query[Security]
        .option
        .transact(xa)

    override def putSecurity(security: Security): F[Either[PutSecurityError, Unit]] =
      fr"insert into $securitiesTableName ($fullSecurityFields) values(${security.id},${security.securityId}, ${security.name}, ${security.registrationNumber}, ${security.emitentTitle})".update.run
        .map(_ => ())
        .transact(xa)
        .attemptSomeSqlState { case sqlstate.class23.UNIQUE_VIOLATION => PutSecurityResponses.SecurityAlreadyExists }

    override def deleteSecurity(id: Int): F[Either[DeleteSecurityError, Unit]] =
      sql"delete from $securitiesTableName where id = $id".update.run
        .transact(xa)
        .map {
          case 0 => DeleteSecurityResponses.SecurityDoesNotExist.asLeft[Unit]
          case _ => Right[DeleteSecurityError, Unit](())
        }

    private val securitiesTableName = fr"securities"
    private val insertSecurityFields = fr"sec_id, sec_name, regnumber, emitent_title"
    private val fullSecurityFields = fr"id, " ++ insertSecurityFields
  }
}
