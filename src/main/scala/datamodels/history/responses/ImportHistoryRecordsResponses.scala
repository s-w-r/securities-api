package datamodels.history.responses

import datamodels.history.HistoryRecordId

object ImportHistoryRecordsResponses {
  final case class ImportHistoryRecordsResponse(success: List[HistoryRecordId], failed: List[HistoryRecordId])
}
