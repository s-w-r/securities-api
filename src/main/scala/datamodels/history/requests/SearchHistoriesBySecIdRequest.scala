package datamodels.history.requests

import datamodels.common.PaginationOptions
import io.circe.generic.semiauto.*

final case class SearchHistoriesBySecIdRequest(
    securityId: String,
    paginationOptions: Option[PaginationOptions]
)
