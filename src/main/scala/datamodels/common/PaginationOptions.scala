package datamodels.common

import datamodels.common.PrimitiveTypes.NonNegativeInt

final case class PaginationOptions(limit: Option[NonNegativeInt], offset: Option[NonNegativeInt])
