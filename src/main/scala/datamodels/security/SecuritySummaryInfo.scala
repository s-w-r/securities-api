package datamodels.security

import datamodels.history.HistoryRecord

final case class SecuritySummaryInfo(security: Security, history: List[HistoryRecord])
