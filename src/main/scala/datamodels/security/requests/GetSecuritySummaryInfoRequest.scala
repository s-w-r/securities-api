package datamodels.security.requests

import datamodels.common.PaginationOptions

final case class GetSecuritySummaryInfoRequest(id: Int, paginationOptions: Option[PaginationOptions])
