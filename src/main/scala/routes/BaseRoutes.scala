package routes

import java.time.LocalDate

import datamodels.history.HistoryRecordId
import sttp.tapir.*

object BaseRoutes {
  val Version: String = "v1"

  val baseEndpointV1: PublicEndpoint[Unit, Unit, Unit, Any] = endpoint.in("securities" / Version)

  val historyIdInput: EndpointInput[HistoryRecordId] = path[String]("secId")
    .and(path[LocalDate]("date"))
    .map(input => HistoryRecordId(input._1, input._2))(id => (id.securityId, id.tradeDate))
}
